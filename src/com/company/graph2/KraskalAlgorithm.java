package com.company.graph2;

import java.util.ArrayList;
import java.util.Collections;

public class KraskalAlgorithm {
    private final ArrayList<Edge> edges = new ArrayList<>();


    public Integer start(int[][] adjacencyMatrix) {
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            for (int j = i; j < adjacencyMatrix.length; j++) {
                if (adjacencyMatrix[i][j] != 0) {
                    edges.add(new Edge(new int[]{i, j}, adjacencyMatrix[i][j]));
                }
            }
        }
        Collections.sort(edges);
        boolean[] vertexesIsPicked = new boolean[adjacencyMatrix.length];
        int countOfEdges = 0;
        int countOfVertexes = 0;
        int vertex1, vertex2;
        int i = 0;
        Edge currEdge;
        int answer = 0;
        while (countOfEdges < adjacencyMatrix.length - 1) {
            currEdge = edges.get(i);
            vertex1 = currEdge.vertexes[0];
            vertex2 = currEdge.vertexes[1];
            if (vertexesIsPicked[vertex1] && vertexesIsPicked[vertex2]) {
                if (countOfVertexes > countOfEdges + 1) {
                    answer += currEdge.weight;
                    countOfEdges++;
                }
                i++;
                continue;
            }
            if (!vertexesIsPicked[vertex1] && !vertexesIsPicked[vertex2]) {
                countOfVertexes += 2;
            }
            if (vertexesIsPicked[vertex1] || vertexesIsPicked[vertex2]) {
                countOfVertexes++;
            }
            countOfEdges++;
            answer += currEdge.weight;
            vertexesIsPicked[vertex1] = true;
            vertexesIsPicked[vertex2] = true;
            i++;
        }
        return answer;
    }

    private class Edge implements Comparable<Edge> {
        private final int[] vertexes;
        private final int weight;

        public Edge(int[] vertexes, int weight) {
            this.vertexes = vertexes;
            this.weight = weight;
        }

        @Override
        public int compareTo(Edge o) {
            return Integer.compare(this.weight, o.weight);
        }
    }
}
