package com.company.graph2;

import java.util.HashMap;
import java.util.Map;

public class DijkstraAlgorithm {

    public HashMap<Integer, Integer> goDijkstra(int[][] adjacencyMatrix, int startIndex) {
        HashMap<Integer, Integer> vertexes = new HashMap<>();
        vertexes.put(startIndex, 0);
        int[] dijkstra = new int[adjacencyMatrix.length];
        boolean[] signsOfVertexes = new boolean[adjacencyMatrix.length];
        dijkstra[startIndex] = 0;
        signsOfVertexes[startIndex] = true;
        for (int i = 0; i < adjacencyMatrix.length; i++) {
            if (i != startIndex) {
                dijkstra[i] = Integer.MAX_VALUE;
            }
        }
        int indexOfCurrentVertex = startIndex;
        for (int i = 0; i < adjacencyMatrix.length - 1; i++) {
            for (int j = 0; j < adjacencyMatrix.length; j++) {
                if (!signsOfVertexes[j] && adjacencyMatrix[indexOfCurrentVertex][j] != 0) {
                    dijkstra[j] = Math.min(dijkstra[j], dijkstra[indexOfCurrentVertex] + adjacencyMatrix[indexOfCurrentVertex][j]);
                }
            }
            int minValue = Integer.MAX_VALUE;
            for (int j = 0; j < dijkstra.length; j++) {
                if (!signsOfVertexes[j] && minValue > dijkstra[j]) {
                    indexOfCurrentVertex = j;
                    minValue = dijkstra[j];
                }
            }
            signsOfVertexes[indexOfCurrentVertex] = true;
            vertexes.put(indexOfCurrentVertex, dijkstra[indexOfCurrentVertex]);
        }
        return vertexes;
    }
}
