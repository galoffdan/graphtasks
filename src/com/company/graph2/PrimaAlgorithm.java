package com.company.graph2;

import java.util.HashSet;

public class PrimaAlgorithm {

    public int start(int[][] adjacencyMatrix) {
        int minEdge;
        int answer = 0;
        int nextVertex = 0;
        int edge;
        HashSet<Integer> addedVertexes = new HashSet<>();
        addedVertexes.add(5);
        for (int i = 0; i < adjacencyMatrix.length - 1; i++) {
            minEdge = Integer.MAX_VALUE;
            for (Integer currentVertex : addedVertexes) {
                for (int j = 0; j < adjacencyMatrix.length; j++) {
                    edge = adjacencyMatrix[currentVertex][j];
                    if (edge != 0 && !addedVertexes.contains(j) && minEdge > edge) {
                        minEdge = edge;
                        nextVertex = j;
                    }
                }
            }
            addedVertexes.add(nextVertex);
            answer += minEdge;
        }
        return answer;
    }
}
