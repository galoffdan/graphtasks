package com.company.graph2;

import java.util.HashMap;

public class GraphTasks2Solution implements GraphTasks2{
    @Override
    public HashMap<Integer, Integer> dijkstraSearch(int[][] adjacencyMatrix, int startIndex) {
        return new DijkstraAlgorithm().goDijkstra(adjacencyMatrix, startIndex);
    }

    @Override
    public Integer primaAlgorithm(int[][] adjacencyMatrix) {
        return new PrimaAlgorithm().start(adjacencyMatrix);
    }

    @Override
    public Integer kraskalAlgorithm(int[][] adjacencyMatrix) {
        return new KraskalAlgorithm().start(adjacencyMatrix);
    }
}
